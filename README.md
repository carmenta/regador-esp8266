# Regador de Jardín con **ESP8266** y MQTT#

Proyecto basado en la librería **Homie** 2.0 para Arduino IDE.

### How do I get set up? ###

* Instalar Arduino IDE
* Instalar Homie 2.0 y sus dependencias (https://homie-esp8266.readme.io/docs/getting-started)
* Imprimir el PCB para modulo ESP8266 ESP-03 ( pcb/ESP-03 ESP8266-Regador.rar ) usando Eagle 6
![2016-11-30 11_29_58-2 Board - C__Users_carmenta_Documents_eagle_ESP-03 ESP8266-Regador_esp-03-BreakO.png](https://bitbucket.org/repo/MEMb45/images/3300642841-2016-11-30%2011_29_58-2%20Board%20-%20C__Users_carmenta_Documents_eagle_ESP-03%20ESP8266-Regador_esp-03-BreakO.png)