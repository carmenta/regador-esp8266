#include <Homie.h>

const int PIN_LED = 14;
const int PIN_REGADOR_1 = 13;
const int PIN_SW_LIMIT = 12;
const int PIN_RESET = 2;
bool status_regador=0;
const bool DEBUG=0;

HomieNode ledNode("led", "switch");
HomieNode regadorNode("regador", "switch");

bool lightOnHandler(const HomieRange& range, const String& value) {
  if (value != "true" && value != "false") return false;

  bool on = (value == "true");
  digitalWrite(PIN_LED, on ? HIGH : LOW);
  ledNode.setProperty("on").send(value);
  if(DEBUG==1){
    Serial << "LED is " << (on ? "on" : "off") << endl;
  }
  return true;
}

bool lightOnHandler_1(const HomieRange& range, const String& value) {
  if (value != "true" && value != "false") return false;  
  bool on = (value == "true");
  status_regador=on;
  digitalWrite(PIN_REGADOR_1, HIGH);
  delay(200);
  regadorNode.setProperty("on").send(value);
  if(DEBUG==1){
    Serial << "Sistema de Riego esta: " << (on ? "on" : "off") << endl;
    Serial << "SW Limit esta: " << (digitalRead(PIN_SW_LIMIT) ? "on" : "off") << endl;
  }
  return true;
}

void blink() {   
  if(status_regador==1 && digitalRead(PIN_SW_LIMIT)==1){
    digitalWrite(PIN_REGADOR_1, LOW);
    if(DEBUG==1){
      Serial << "ISR info, status_regador = 1" << endl;
      Serial << "La valvula (SW Limit ISR) está: " << (digitalRead(PIN_SW_LIMIT) ? "on" : "off") << endl; 
    }
  }    
  if(status_regador==0 && digitalRead(PIN_SW_LIMIT)==0){
    digitalWrite(PIN_REGADOR_1, LOW);
    if(DEBUG==1){
      Serial << "ISR info, status_regador = 0" << endl;
      Serial << "La valvula (SW Limit ISR) está: " << (digitalRead(PIN_SW_LIMIT) ? "on" : "off") << endl; 
    }
  } 
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  Serial << "Iniciando Regador MQTT =) ...";
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_REGADOR_1, OUTPUT);
  pinMode(PIN_SW_LIMIT, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_SW_LIMIT), blink, CHANGE);
  pinMode(PIN_RESET, INPUT);
  Homie_setFirmware("regador-jardin", "1.0.0");
  ledNode.advertise("on").settable(lightOnHandler);
  regadorNode.advertise("on").settable(lightOnHandler_1);
    
  Homie.setResetTrigger(PIN_RESET, LOW, 6000);
  Homie.disableLedFeedback();
  Homie.__setBrand("LightWorm_I"); 
  Homie.setup();
  status_regador=0;
  digitalWrite(PIN_REGADOR_1, HIGH);
}

void loop() {    
  Homie.loop();
}
